package edu.uoc.android.restservice.rest.Modelo;

import com.google.gson.annotations.SerializedName;

public class Profesor {

    @SerializedName("nombre")
    private String nombre;

    @SerializedName("apellido")
    private String apellido;

    @SerializedName("titulo")
    private String titulo;

    @SerializedName("materia")
    private String materia;

    @SerializedName("imagen")
    private String imagen;

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getMateria() {
        return materia;
    }

    public String getImagen() {
        return imagen;
    }
}
