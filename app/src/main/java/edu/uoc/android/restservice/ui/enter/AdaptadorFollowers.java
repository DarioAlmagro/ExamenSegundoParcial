package edu.uoc.android.restservice.ui.enter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.Modelo.Estudiante;

/**
 * Created by edgardopanchana on 4/29/18.
 */

public class AdaptadorFollowers extends RecyclerView.Adapter<AdaptadorFollowers.ViewHolderFollowers> {

    ArrayList<Estudiante> listaEstudiantes;

    public AdaptadorFollowers(ArrayList<Estudiante> listaFollowers) {
        this.listaEstudiantes = listaFollowers;
    }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        return new ViewHolderFollowers(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, int position) {
        holder.textViewNombres.setText(listaEstudiantes.get(position).getNombres());
        holder.textViewApellidos.setText(listaEstudiantes.get(position).getApellidos());
        holder.textViewParcialUno.setText(listaEstudiantes.get(position).getParcial_uno());
        holder.textViewParcialDos.setText(listaEstudiantes.get(position).getParcial_dos());
        holder.textViewAprueba.setText(listaEstudiantes.get(position).getAprueba());

    }

    @Override
    public int getItemCount() {
        return listaEstudiantes.size();
        //borrar el return 1;
       // return 1;
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {

        TextView textViewNombres, textViewApellidos, textViewParcialUno, textViewParcialDos, textViewAprueba;


        public ViewHolderFollowers(View itemView) {
            super(itemView);

            textViewNombres = (TextView) itemView.findViewById(R.id.textViewNombres);
            textViewApellidos = (TextView) itemView.findViewById(R.id.textViewApellidos);
            textViewParcialUno = (TextView) itemView.findViewById(R.id.textViewParcialUno);
            textViewParcialDos = (TextView) itemView.findViewById(R.id.textViewParcialDos);
            textViewAprueba = (TextView) itemView.findViewById(R.id.textViewAprueba);
        }
    }
}
